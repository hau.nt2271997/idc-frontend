import en from '../lang/en.js'
import vi from '../lang/vi.js'

export const I18N = {
  locales: [
    {
      code: 'en',
      iso: 'en',
      name: 'English'
    },
    {
      code: 'vi',
      iso: 'vi',
      name: 'Vietnamese'
    }
  ],
  defaultLocale: 'vi',
  routes: {
    about: {
      fr: '/a-propos',
      en: '/about-us'
    },
    posts: {
      fr: '/articles'
    },
    'post/_id': {
      fr: '/article/:id?',
      es: '/articulo/:id?'
    },
    'dynamicNested/_category': {
      fr: 'imbrication-dynamique/:category'
    }
  },
  vueI18n: {
    fallbackLocale: 'vi',
    messages: { en, vi }
  }
}
